function FiltersHandler(options) {
  this.products = options.products;
  this.$filtersContainer = $(options.filtersSelector);
  this.filters = options.filters;
  this.sortings = options.sortings;
  this.paginations = options.paginations;
  this.productsHandler = new ProductsHandler(options.productsSelector);
  this.defaultValues = options.defaultValues;
  this.$paginationSelect = null;
  this.$sortingSelect = null;
}

FiltersHandler.prototype.setHash = function(hashValue){
  window.location.hash = hashValue;
}

FiltersHandler.prototype.getHash = function(){
  return window.location.hash;
}

FiltersHandler.prototype.setDefaultValues = function() {
  try {
    var tempObject = JSON.parse(decodeURI(this.getHash().substr(1)));
    for (property in tempObject) {
      if (this.defaultValues.hasOwnProperty(property)) {
        if (property == "filter") {
          for (prop in tempObject.filter) {
            if (this.defaultValues.filter.hasOwnProperty(prop)) {
              this.defaultValues.filter[prop] = tempObject.filter[prop];
            }
          }
        } else {
          this.defaultValues[property] = tempObject[property];
        }
      }
    }
  } catch {
    this.setHash(JSON.stringify(this.defaultValues));
  }
}

FiltersHandler.prototype.init = function() {
  this.setFilterObject();
  this.setDefaultValues();
  this.$filtersContainer.append(this.getHTMLStructure());
  this.bindEvents();
  this.productsHandler.init(this.getFilteredProducts(), this.setCurrentPage.bind(this), parseInt(this.defaultValues.currentPage));
};

FiltersHandler.prototype.setCurrentPage = function(value) {
  this.defaultValues.currentPage = value;
    this.setHash(JSON.stringify(this.defaultValues));
}

FiltersHandler.prototype.setFilterObject = function() {
  var filters = this.filters;
  var filterObject = {};
  var filterTypes = Object.keys(filters);
  for (var i = 0; i < filterTypes.length; i++) {
    filterObject[filterTypes[i]] = {};
  }
  this.products.forEach(function(item, index, array) {
    var outerItem = item;
    for (var i = 0; i < filterTypes.length; i++) {
      filterObject[filterTypes[i]][outerItem[filterTypes[i]]] = 1;
    }
  });
  Object.keys(filters).forEach(function(item, index, array) {
    filters[item].values = Object.keys(filterObject[item]);
  });
};

FiltersHandler.prototype.getHTMLStructure = function() {
  var $filtersDiv = this.getFiltersStructure();
  this.$paginationSelect = this.getPaginationSelectStructure();
  $filtersDiv.append(this.$paginationSelect);
  this.$sortingSelect = this.getSortingSelectStructure();
  $filtersDiv.append(this.$sortingSelect);
  return $filtersDiv;
};

FiltersHandler.prototype.getFiltersStructure = function() {
  var that = this;
  var filters = this.filters;
  var $filtersDiv = $("<div data-type='filters-div'/>");
  Object.keys(filters).forEach(function(item, index, array) {
    var $filterCheckboxes = $("<div data-type='filter-div' class='filter-div'/>");
    $filterCheckboxes.append("<div>" + filters[item]["name"] + "</div>");
    var filterType = item;
    var checked = "";
    if (filters[item]["type"] === "Boolean") {
      checked = "";
      if (that.defaultValues.filter[item].length > 0) {
        checked = "checked";
      }
      $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + item + " data-type=" + item + " " + checked + ">" + filters[item]["name"] + "<br>");
    } else {
      filters[item]["values"].forEach(function(item, index, array) {
        checked = "";
        if (that.defaultValues.filter[filterType].indexOf(item) != -1) {
          checked = "checked";
        }
        $filterCheckboxes.append("<input type='checkbox' value='" + item + "' name=" + filterType + " data-type=" + filterType + " " + checked + ">" + item + "<br>");
      });
    }
    $filterCheckboxes.append("<br>");
    $filtersDiv.append($filterCheckboxes);
  });
  return $filtersDiv;
}

FiltersHandler.prototype.getSortingSelectStructure = function() {
  var $sortingSelect = $("<select data-type='sorting'/>");
  this.sortings.forEach(function(item, index, array) {
    $sortingSelect.append("<option data-value=" + item + ">" + item + "</option>");
  });
  this.productsHandler.currentSortingBy = this.defaultValues.sorting;
  $sortingSelect.find("option[data-value=" + this.defaultValues.sorting + "]").attr("selected", "selected");
  return $sortingSelect;
}


FiltersHandler.prototype.getPaginationSelectStructure = function() {
  var $paginationSelect = $("<select data-type='pagination'/>");
  this.paginations.forEach(function(item, index, array) {
    $paginationSelect.append("<option data-value=" + item + ">" + item + "</option>");
  });
  this.productsHandler.productsPerPage = this.defaultValues.pagination;
  $paginationSelect.find("option[data-value=" + this.defaultValues.pagination + "]").attr("selected", "selected");
  return $paginationSelect;
}

FiltersHandler.prototype.getFilteredProducts = function() {
  var filters = this.filters;
  var filteredProducts = this.products.slice(0);
  var that = this;
  Object.keys(filters).forEach(function(item, index, array) {
    var filterType = item;
    var checkedElements = that.$filtersContainer.find("input[data-type=" + item + "]:checked");
    if (checkedElements.length === 0) {
      return;
    }
    if (filters[item]["type"] == "Boolean") {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        return item[filterType] == false;
      });
    } else {
      filteredProducts = filteredProducts.filter(function(item, index, array) {
        for (var i = 0; i < checkedElements.length; i++) {
          if (item[filterType] == checkedElements[i].value) {
            return true;
          }
        }
        return false;
      });
    }
  });
  return filteredProducts;
};

FiltersHandler.prototype.bindEvents = function() {
  var that = this;
  this.$filtersContainer.find("input[type=checkbox]").click(function() {
    that.updateHash($(this));
    that.productsHandler.updateProducts(that.getFilteredProducts());
  });

  this.$paginationSelect.on("change", function() {
    that.updatePaginationHash($(this));
    that.productsHandler.updateProductsPerPage(parseInt(this.value));
  });

  this.$sortingSelect.on("change", function() {
    that.updateSortingHash($(this));
    that.productsHandler.sortProductsBy(this.value);
  });
};

FiltersHandler.prototype.updatePaginationHash = function(element) {
  this.defaultValues[element.attr("data-type")] = parseInt(element.val());
  this.setHash(JSON.stringify(this.defaultValues));
}

FiltersHandler.prototype.updateSortingHash = function(element) {
  this.defaultValues[element.attr("data-type")] = element.val();
  this.setHash(JSON.stringify(this.defaultValues));
}

FiltersHandler.prototype.updateHash = function(element) {
  var filterArray = this.defaultValues.filter[element.attr("data-type")];
  var index = filterArray.indexOf(element.val());
  if (index !== -1) {
    filterArray.splice(index, 1);
  } else {
    filterArray.push(element.val());
  }
  this.setHash(JSON.stringify(this.defaultValues));
}