function Controller() {}

Controller.prototype.init = function() {
  this.options = {
    filters: {
      color: {
        type: "String",
        name: "Colors"
      },
      brand: {
        type: "String",
        name: "Brands"
      },
      sold_out: {
        type: "Boolean",
        name: "Available"
      }
    },
    defaultValues: {
      filter: {
        color: [],
        brand: [],
        sold_out: []
      },
      pagination: 6,
      sorting: "color",
      currentPage: 1
    },
    paginations: [3, 6, 12, 20],
    sortings: ["name", "brand", "color", "sold_out"],
    filtersSelector: "[data-type=filters-container]",
    productsSelector: "[data-type=products-container]"
  };
  var ajaxHandler = new AJAXHandler("product.json", this.loadContent.bind(this), function() {});
  ajaxHandler.init();

}

Controller.prototype.loadContent = function(response) {
  this.options.products = response;
  var filtersHandler = new FiltersHandler(this.options);
  filtersHandler.init();
};

$(document).ready(function() {
  var controller = new Controller();
  controller.init();
});