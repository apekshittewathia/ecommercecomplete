function ProductsHandler(productsSelector) {
  this.products = null; //array
  this.productsPerPage = null; //int
  this.numberOfPages = null; //int
  this.currentPage = null; //int
  this.currentSortingBy = null; //string
  this.$productsContainer = $(productsSelector);
}

ProductsHandler.prototype.init = function(products, setCurrentPageCallback, currentPage) {
  this.products = products;
  this.setCurrentPageCallback = setCurrentPageCallback;
  this.currentPage = currentPage;
  this.update(true);
  this.bindEvents();
};

ProductsHandler.prototype.updateProductsPerPage = function(productsPerPage) {
  this.productsPerPage = productsPerPage;
  //no need to sort, hence not calling this.update()
  this.updatePages();
  this.showProducts();
};

ProductsHandler.prototype.updateProducts = function(products) {
  this.products = products;
  this.update();
};

ProductsHandler.prototype.sortProductsBy = function(type) {
  this.currentSortingBy = type;
  this.update();
};

ProductsHandler.prototype.update = function(init = false) {
  this.updatePages(init);
  this.sortProducts();
  this.showProducts();
};

ProductsHandler.prototype.sortProducts = function() {
  var type = this.currentSortingBy;
  this.products.sort(function(a, b) {
    if (a[type] > b[type]) {
      return 1;
    }
    if (a[type] < b[type]) {
      return -1;
    }
    return 0;
  });
};

ProductsHandler.prototype.updatePages = function(init) {
  if (!init) {
    this.setCurrentPage(1);
  }
  this.numberOfPages = this.products.length / this.productsPerPage;
  this.$productsContainer.empty();
  this.$productsContainer.append(this.getPaginationStructure());
};

ProductsHandler.prototype.showProducts = function() {
  this.$productsContainer.children("div[data-type=products-div]").remove();
  this.$productsContainer.prepend(this.getProductsStructure());
};

ProductsHandler.prototype.getProductsStructure = function() {
  var $productsDiv = $("<div data-type='products-div'/>");
  var startIndex = (this.currentPage - 1) * this.productsPerPage;
  var endIndex = startIndex + this.productsPerPage;
  this.products.slice(startIndex, endIndex).forEach(function(item, index, array) {
    var $productDiv = $("<div class='product-div'/>");
    $productDiv.append("<img src='product_data/images/" + item.url + "'>");
    $productDiv.append("<div>" + item.name + "</div>");
    $productDiv.append("<div>" + item.color + "</div>");
    $productDiv.append("<div>" + item.brand + "</div>");
    $productsDiv.append($productDiv);
  });
  return $productsDiv;
};

ProductsHandler.prototype.getPaginationStructure = function() {
  var paginationDiv = $("<div class='pagination' data-type='pagination'/>");
  for (var i = 0; i < this.numberOfPages; i++) {
    paginationDiv.append("<a href='' value=" + (i + 1) + ">" + (i + 1) + "</a>");
  }
  paginationDiv.find("a").eq(this.currentPage - 1).css("background-color", "red");
  return paginationDiv;
};

ProductsHandler.prototype.setCurrentPage = function(value) {
  this.currentPage = value;
  this.setCurrentPageCallback(value);
}

ProductsHandler.prototype.bindEvents = function() {
  var that = this;
  this.$productsContainer.on("click", "div[data-type=pagination] a", function(event) {
    event.preventDefault();
    that.setCurrentPage(parseInt(this.getAttribute("value")));
    var $this = $(this);
    $this.css("background-color", "red");
    $this.siblings().css("background-color", "");
    that.showProducts();
  });
};