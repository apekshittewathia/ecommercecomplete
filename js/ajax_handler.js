function AJAXHandler(filePath, successCallback, failureCallback) {
  this.filePath = filePath;
  this.successCallback = successCallback;
  this.failureCallback = failureCallback;
}

AJAXHandler.prototype.init = function() {
  var that = this;
  $.ajax({
    url: this.filePath,
    type: "GET",
    dataType: "json"
  }).done(function(response) {
    that.response = response;
    that.successCallback(response);
  }).fail(function(xhr, status, errorThrown) {
    that.failureCallback();
    alert("Sorry, there was a problem!");
    console.log("Error: " + errorThrown);
    console.log("Status: " + status);
    console.log(xhr);
  });
};